package ru.itis.javalab.testtask.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author Киямдинов Ильдар
 * @project test-task
 * @created 26.03.2022
 */

public class LoginValidator implements ConstraintValidator<ValidLogin, String> {

    @Override
    public boolean isValid(String login, ConstraintValidatorContext context) {
        return login.matches("^[a-zA-Z0-9_-]{3,16}$");
    }

}

package ru.itis.javalab.testtask.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Киямдинов Ильдар
 * @project test-task
 * @created 26.03.2022
 */

@Constraint(validatedBy = LoginValidator.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidLogin {

    String message() default "Логин может содеражть только латинские буквы, цифры, тире, знак подчеркивания и иметь длину от 3 до 16 символов";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}

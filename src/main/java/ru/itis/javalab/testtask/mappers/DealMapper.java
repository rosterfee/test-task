package ru.itis.javalab.testtask.mappers;

import org.mapstruct.Mapper;
import ru.itis.javalab.testtask.dto.DealDto;
import ru.itis.javalab.testtask.dto.DealForm;
import ru.itis.javalab.testtask.entities.Deal;
import ru.itis.javalab.testtask.entities.User;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Киямдинов Ильдар
 * @project test-task
 * @created 26.03.2022
 */

@Mapper
public abstract class DealMapper {

    public abstract Deal dealFormToDealMapper(DealForm dealForm);

    public DealDto dealToDto(Deal deal) {

        DealDto dealDto = new DealDto();

        dealDto.setTitle(deal.getTitle());
        dealDto.setStartsAt(deal.getStartsAt());
        dealDto.setEndsAt(deal.getEndsAt());

        Set<Long> userIds = deal.getUsers().stream().map(User::getId).collect(Collectors.toSet());
        dealDto.setUserIds(userIds);

        return dealDto;
    }

}

package ru.itis.javalab.testtask.mappers;

import org.mapstruct.Mapper;
import ru.itis.javalab.testtask.dto.UserDto;
import ru.itis.javalab.testtask.dto.UserForm;
import ru.itis.javalab.testtask.entities.User;

/**
 * @author Киямдинов Ильдар
 * @project test-task
 * @created 26.03.2022
 */

@Mapper
public interface UserMapper {

    User userFormToUser(UserForm userForm);
    UserDto userToDto(User user);

}

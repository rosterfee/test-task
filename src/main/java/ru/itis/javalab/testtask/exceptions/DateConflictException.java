package ru.itis.javalab.testtask.exceptions;

/**
 * @author Киямдинов Ильдар
 * @project test-task
 * @created 26.03.2022
 */

public class DateConflictException extends RuntimeException {

    public DateConflictException(String message) {
        super(message);
    }

}

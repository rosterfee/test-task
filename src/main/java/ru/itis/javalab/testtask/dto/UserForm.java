package ru.itis.javalab.testtask.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.javalab.testtask.validation.ValidLogin;

import javax.validation.constraints.NotBlank;

/**
 * @author Киямдинов Ильдар
 * @project test-task
 * @created 26.03.2022
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserForm {

    @NotBlank
    private String name;

    @NotBlank
    @ValidLogin
    private String login;

}

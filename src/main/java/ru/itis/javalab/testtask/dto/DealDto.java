package ru.itis.javalab.testtask.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Set;

/**
 * @author Киямдинов Ильдар
 * @project test-task
 * @created 28.03.2022
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DealDto {

    private String title;

    private LocalDateTime startsAt;

    private LocalDateTime endsAt;

    private Set<Long> userIds;

}

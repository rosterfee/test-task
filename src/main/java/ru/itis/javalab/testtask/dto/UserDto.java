package ru.itis.javalab.testtask.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Киямдинов Ильдар
 * @project test-task
 * @created 28.03.2022
 */

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class UserDto {

    private String name;

    private String login;

}

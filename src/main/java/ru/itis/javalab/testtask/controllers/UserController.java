package ru.itis.javalab.testtask.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.javalab.testtask.dto.UserDto;
import ru.itis.javalab.testtask.dto.UserForm;
import ru.itis.javalab.testtask.entities.User;
import ru.itis.javalab.testtask.services.UserService;

import javax.validation.Valid;

/**
 * @author Киямдинов Ильдар
 * @project test-task
 * @created 26.03.2022
 */

@RestController
@RequestMapping("user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public ResponseEntity<UserDto> createUser(@Valid @RequestBody UserForm userForm) {

        UserDto createdUser = userService.createUser(userForm);
        return ResponseEntity.ok(createdUser);

    }

}

package ru.itis.javalab.testtask.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.javalab.testtask.dto.DealDto;
import ru.itis.javalab.testtask.dto.DealForm;
import ru.itis.javalab.testtask.services.DealService;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Киямдинов Ильдар
 * @project test-task
 * @created 26.03.2022
 */

@RestController
@RequestMapping("deal")
public class DealController {

    private final DealService dealService;

    public DealController(DealService dealService) {
        this.dealService = dealService;
    }

    @PostMapping
    public ResponseEntity<DealDto> attachDealToUsers(@RequestBody DealForm dealForm) {

        DealDto dealDto = dealService.addDeal(dealForm);
        return ResponseEntity.ok(dealDto);
    }

    @GetMapping
    public ResponseEntity<List<LocalDateTime[]>> getCorporateFreeGaps(@RequestBody List<Long> ids) {
        return ResponseEntity.ok(dealService.getCorporateFreeGaps(ids));
    }
}

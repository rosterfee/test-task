package ru.itis.javalab.testtask.utils;

import ru.itis.javalab.testtask.entities.Deal;

import java.util.Comparator;
import java.util.List;

/**
 * @author Киямдинов Ильдар
 * @project test-task
 * @created 27.03.2022
 */

public class DealListsComparator implements Comparator<List<Deal>> {

    @Override
    public int compare(List<Deal> o1, List<Deal> o2) {
        return o2.size() - o1.size();
    }

}

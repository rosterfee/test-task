package ru.itis.javalab.testtask.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.itis.javalab.testtask.entities.Deal;
import ru.itis.javalab.testtask.entities.User;

import java.util.Set;

/**
 * @author Киямдинов Ильдар
 * @project test-task
 * @created 26.03.2022
 */

public interface DealsRepository extends JpaRepository<Deal, Long> {



}

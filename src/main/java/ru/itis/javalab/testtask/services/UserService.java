package ru.itis.javalab.testtask.services;

import ru.itis.javalab.testtask.dto.UserDto;
import ru.itis.javalab.testtask.dto.UserForm;
import ru.itis.javalab.testtask.entities.User;

/**
 * @author Киямдинов Ильдар
 * @project test-task
 * @created 26.03.2022
 */

public interface UserService {

    UserDto createUser(UserForm userForm);

}

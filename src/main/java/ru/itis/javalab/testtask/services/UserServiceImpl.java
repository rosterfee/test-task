package ru.itis.javalab.testtask.services;

import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Service;
import ru.itis.javalab.testtask.dto.UserDto;
import ru.itis.javalab.testtask.dto.UserForm;
import ru.itis.javalab.testtask.entities.User;
import ru.itis.javalab.testtask.mappers.UserMapper;
import ru.itis.javalab.testtask.repos.UsersRepository;

/**
 * @author Киямдинов Ильдар
 * @project test-task
 * @created 26.03.2022
 */

@Service
public class UserServiceImpl implements UserService {

    private final UsersRepository usersRepository;

    private final UserMapper mapper = Mappers.getMapper(UserMapper.class);

    public UserServiceImpl(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public UserDto createUser(UserForm userForm) {
        User user = mapper.userFormToUser(userForm);
        User savedUser = usersRepository.save(user);
        return mapper.userToDto(savedUser);
    }

}

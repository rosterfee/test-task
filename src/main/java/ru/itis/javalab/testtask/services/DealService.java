package ru.itis.javalab.testtask.services;

import ru.itis.javalab.testtask.dto.DealDto;
import ru.itis.javalab.testtask.dto.DealForm;
import ru.itis.javalab.testtask.entities.Deal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Киямдинов Ильдар
 * @project test-task
 * @created 26.03.2022
 */

public interface DealService {

    DealDto addDeal(DealForm dealForm);

    List<LocalDateTime[]> getCorporateFreeGaps(List<Long> ids);

}

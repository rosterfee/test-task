package ru.itis.javalab.testtask.services;

import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Service;
import ru.itis.javalab.testtask.dto.DealDto;
import ru.itis.javalab.testtask.dto.DealForm;
import ru.itis.javalab.testtask.entities.Deal;
import ru.itis.javalab.testtask.entities.User;
import ru.itis.javalab.testtask.exceptions.DateConflictException;
import ru.itis.javalab.testtask.mappers.DealMapper;
import ru.itis.javalab.testtask.repos.DealsRepository;
import ru.itis.javalab.testtask.repos.UsersRepository;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Киямдинов Ильдар
 * @project test-task
 * @created 26.03.2022
 */

@Service
public class DealServiceImpl implements DealService {

    private final UsersRepository usersRepository;
    private final DealsRepository dealsRepository;

    private final DealMapper mapper = Mappers.getMapper(DealMapper.class);
    private final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");

    public DealServiceImpl(UsersRepository usersRepository, DealsRepository dealsRepository) {
        this.usersRepository = usersRepository;
        this.dealsRepository = dealsRepository;
    }

    @Override
    public DealDto addDeal(DealForm dealForm) {

        Deal deal = mapper.dealFormToDealMapper(dealForm);
        Set<User> dealUsers = new HashSet<>();
        User user;

        //Время начала добавляемого дела
        LocalDateTime newDealStartsAt = dealForm.getStartsAt();

        //Время конца добавляемого дела
        LocalDateTime newDealEndsAt = dealForm.getEndsAt();

        LocalDateTime oldDealStartsAt;
        LocalDateTime oldDealEndsAt;
        //Для каждого id'шника из формы
        for (Long id: dealForm.getUserIds()) {

            //Вытаскиваем соответствующего юзера из бд
            user = usersRepository.findById(id).orElseThrow(
                    () -> new EntityNotFoundException("Пользователь с id " + id + " не найден"));

            //И проверяю, что у него нет запланированных дел, которые пересекаются с новым
            for (Deal userDeal: user.getDeals()) {

                oldDealStartsAt = userDeal.getStartsAt();
                oldDealEndsAt = userDeal.getEndsAt();

                //Если есть хоть одно такое дело, выкидываю исключение
                if ((newDealStartsAt.isAfter(oldDealStartsAt) && newDealEndsAt.isBefore(oldDealEndsAt))
                || (newDealEndsAt.isAfter(oldDealStartsAt) && newDealEndsAt.isBefore(oldDealEndsAt))
                || newDealStartsAt.isEqual(oldDealStartsAt) || newDealEndsAt.isEqual(oldDealEndsAt)) {
                    throw new DateConflictException("У пользователя " + user.getName() +
                            " запланировано дело с " + oldDealStartsAt.format(dateFormat) +
                            " до " + oldDealEndsAt.format(dateFormat));
                }
            }
            //Если пересечений нет, то добавляю юзера в список
            dealUsers.add(user);
        }
        //Присваиваю делу список юзеров
        deal.setUsers(dealUsers);

        //Сохраняю и возвращаю новое дело
        Deal savedDeal = dealsRepository.save(deal);
        return mapper.dealToDto(savedDeal);
    }

    @Override
    public List<LocalDateTime[]> getCorporateFreeGaps(List<Long> ids) {

        //Если список id'шников пустой, выбрасываю исклюение
        if (ids.isEmpty()) {
            throw new IllegalArgumentException("Список id пуст");
        }
        //Получаю всех юзеров по списку id'шников
        List<User> users = usersRepository.findAllById(ids);

        List<Deal> sortedUserDeals;
        List<Deal> userGaps;

        //Список для хранения расписания всех юзеров
        List<List<Deal>> freeDealsSchedule = new ArrayList<>();

        int userDealsCount;

        //Для каждого юзера:
        for (User user: users) {

            userDealsCount = user.getDeals().size();

            //М старый список дел на сортированный
            sortedUserDeals = user.getDeals().stream().sorted().collect(Collectors.toList());

            userGaps = new ArrayList<>();

            LocalDateTime nextDealStartsAt;
            LocalDateTime currentDealEndsAt;
            //Если у юзера по крайней мере 2 дела в расписании, создаю список свободных промежутков
            //исходя из сортированного списка дел
            if (userDealsCount > 1) {
                for (int i = 0; i < sortedUserDeals.size() - 1; i++) {

                    currentDealEndsAt = sortedUserDeals.get(i).getEndsAt();
                    nextDealStartsAt = sortedUserDeals.get(i + 1).getStartsAt();

                    //Если время конца текущего дела не совпадает с временем начала следующего дела:
                    if (!currentDealEndsAt.isEqual(nextDealStartsAt)) {

                        //Свободный промежуток = (конец i-го дела ; начало i+1 дела)
                        userGaps.add(Deal.builder()
                                .startsAt(currentDealEndsAt)
                                .endsAt(nextDealStartsAt)
                                .build());
                    }
                }
            }
            //Если у юзера всего одно дело:
            else if (userDealsCount == 1) {

                Deal deal = sortedUserDeals.get(0);

                //Получаю время начала и конца запланированного дела
                LocalDateTime dealStartsAt = deal.getStartsAt();
                LocalDateTime dealEndsAt = deal.getEndsAt();

                //Генерирую полночь этого дня
                LocalDateTime midnight = LocalDateTime.of(dealStartsAt.getYear(),
                        dealStartsAt.getMonth(), dealStartsAt.getDayOfMonth(), 0, 0, 0);

                //Если дело начинается после полуночи, то добавляю в список свободных промежутков
                //время от полуночи до начада дела
                if (dealStartsAt.isAfter(midnight)) {
                    userGaps.add(Deal.builder()
                            .startsAt(midnight)
                            .endsAt(dealStartsAt)
                                .build());
                }
                //генерирую полночь следующего дня
                midnight = midnight.plusDays(1);

                //Если дело кончается до полуночи следующего дня, так же в список свободных промежутков
                //добавляю время от конца дела до полуночи следующего дня
                if (dealEndsAt.isBefore(midnight)) {
                    userGaps.add(Deal.builder()
                            .startsAt(dealEndsAt)
                            .endsAt(midnight)
                            .build());
                }
            }

            //Добавляю в общее расписание список свободных промежутков времени текущего юзера
            freeDealsSchedule.add(userGaps);
        }

        List<Deal> resultCorporateDealsList = new ArrayList<>();

        //Количество юзеров, у которых есть свободные промежутки времени
        int freeDealsScheduleSize = freeDealsSchedule.size();

        //Если таких юзеров несколько:
        if (freeDealsScheduleSize > 1) {

            //Получаю список дел любого юзера (можно взять первого)
            List<Deal> firstUserGaps = freeDealsSchedule.get(0);

            List<Deal> currentUserGaps;
            LocalDateTime currentStartsAt;
            LocalDateTime currentEndsAt;

            //Для каждого свободного промежутка первого юзера:
            for (Deal firstUserGap : firstUserGaps) {

                //Список общих свободных промежутков с данным промежутком первого юзера
                List<Deal> corporateGaps = Collections.singletonList(firstUserGap);

                List<Deal> currentUserCorporateGaps;
                //Для каждого другого юзера:
                for (int i = 1; i < freeDealsSchedule.size(); i++) {

                    //Свободные промежутки текущего юзера
                    currentUserGaps = freeDealsSchedule.get(i);

                    //Общие с данным юзером свободные промежутки
                    currentUserCorporateGaps = new ArrayList<>();

                    //Пробегаюсь по каждому его свободному промежутку
                    for (Deal currentGap : currentUserGaps) {

                        //И для каждого из текущих общих промежутков
                        for (Deal corporateGap: corporateGaps) {

                            LocalDateTime corporateStartsAt = corporateGap.getStartsAt();
                            LocalDateTime corporateEndsAt = corporateGap.getEndsAt();

                            currentStartsAt = currentGap.getStartsAt();
                            currentEndsAt = currentGap.getEndsAt();

                            //Рассматриваю все 4 расположения промежутков относительно друг друга,
                            //когда они имеют общий промежуток

                            //__(__________)__ - общий свободный промежуток
                            //_____(____)_____ - текущий свободный промежуток текущего юзера
                            if ((currentStartsAt.isAfter(corporateStartsAt) || currentStartsAt.isEqual(corporateStartsAt))
                                    && (currentEndsAt.isBefore(corporateEndsAt) || currentEndsAt.isEqual(corporateEndsAt))) {

                                currentUserCorporateGaps.add(Deal.builder()
                                        .startsAt(currentStartsAt)
                                        .endsAt(currentEndsAt)
                                        .build());
                            }
                            //_____(____)_____ - общий свободный промежуток
                            //__(__________)__ - текущий свободный промежуток текущего юзера
                            else if ((currentStartsAt.isBefore(corporateStartsAt) || currentStartsAt.isEqual(corporateStartsAt))
                                    && (currentEndsAt.isAfter(corporateEndsAt)) || currentEndsAt.isEqual(corporateEndsAt)) {

                                currentUserCorporateGaps.add(Deal.builder()
                                        .startsAt(corporateStartsAt)
                                        .endsAt(corporateEndsAt)
                                        .build());
                            }
                            //______(________)______ - общий свободный промежуток
                            //__(________)__ - текущий свободный промежуток текущего юзера
                            else if (currentStartsAt.isBefore(corporateStartsAt) && currentEndsAt.isAfter(corporateStartsAt)
                                    && currentEndsAt.isBefore(corporateEndsAt)) {

                                currentUserCorporateGaps.add(Deal.builder()
                                        .startsAt(corporateStartsAt)
                                        .endsAt(corporateEndsAt)
                                        .build());
                            }
                            //__(________)__ - общий свободный промежуток
                            //______(________)______ - текущий свободный промежуток текущего юзера
                            else if (currentStartsAt.isAfter(corporateStartsAt) && currentStartsAt.isBefore(corporateEndsAt)
                                    && currentEndsAt.isAfter(corporateEndsAt)) {

                                currentUserCorporateGaps.add(Deal.builder()
                                        .startsAt(corporateStartsAt)
                                        .endsAt(corporateEndsAt)
                                        .build());
                            }
                        }
                    }
                    //Если у юзера нашлись общие промежутки с текущими общими промежутками,
                    //то обновляем их список
                    if (!currentUserCorporateGaps.isEmpty()) {
                        corporateGaps = currentUserCorporateGaps;
                    }
                }
                //В финальный список добавляем общие свободные промежутки с текущим промежутком первого юзера
                resultCorporateDealsList.addAll(corporateGaps);
            }
        }
        //Если такой юзер один, то просто добавляем его свободные промежутки в финальный список
        else if (freeDealsScheduleSize == 1){
            resultCorporateDealsList.addAll(freeDealsSchedule.get(0));
        }

        //Возвращаю финальный список, где каждый промежуток представлен в виде массива,
        //нулевой элемент которого - это начало промежутка, а первый элемент - конец промежутка
        return resultCorporateDealsList.stream().map(deal -> new LocalDateTime[] {
                        deal.getStartsAt(),
                        deal.getEndsAt()
        }).collect(Collectors.toList());
    }

}

package ru.itis.javalab.testtask.controllers;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import ru.itis.javalab.testtask.dto.UserDto;
import ru.itis.javalab.testtask.dto.UserForm;
import ru.itis.javalab.testtask.entities.User;
import ru.itis.javalab.testtask.services.UserService;

import static org.mockito.Mockito.when;
import static org.hamcrest.Matchers.is;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

/**
 * @author Киямдинов Ильдар
 * @project test-task
 * @created 28.03.2022
 */

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@DisplayName("UserController is working when")
public class UserControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @BeforeEach
    public void setUp() {
        when(userService.createUser(UserForm.builder()
                .login("roSterfee56_")
                .name("Киямдинов Ильдар")
                    .build())).thenReturn(UserDto.builder()
                .login("roSterfee56_")
                .name("Киямдинов Ильдар")
                    .build());
    }

    @Nested
    @DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
    @DisplayName("createUser() is working")
    class CreateUserTests {

        @Test
        public void user_is_created_when_valid_form() throws Exception {
            mockMvc.perform(post("/user")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\n" +
                            "    \"name\": \"Киямдинов Ильдар\",\n" +
                            "    \"login\": \"roSterfee56_\"\n" +
                            "}"))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("name", is("Киямдинов Ильдар")))
                    .andExpect(jsonPath("login", is("roSterfee56_")));
        }

        @Test
        public void bad_request_when_invalid_login() throws Exception {
            mockMvc.perform(post("/user")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content("{\n" +
                                    "    \"name\": \"Киямдинов Ильдар\",\n" +
                                    "    \"login\": \"roSterфee56_\"\n" +
                                    "}"))
                            .andDo(print())
                            .andExpect(status().isBadRequest());
        }

        @Test
        public void bad_request_when_body_is_empty() throws Exception {
            mockMvc.perform(post("/user"))
                    .andDo(print())
                    .andExpect(status().isBadRequest());
        }

    }

}

package ru.itis.javalab.testtask.controllers;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultHandler;
import ru.itis.javalab.testtask.dto.DealDto;
import ru.itis.javalab.testtask.dto.DealForm;
import ru.itis.javalab.testtask.services.DealService;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

import static org.mockito.Mockito.when;
import static org.hamcrest.Matchers.is;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

/**
 * @author Киямдинов Ильдар
 * @project test-task
 * @created 28.03.2022
 */

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@DisplayName("DealController is working when")
public class DealControllerTests {

    private static final LocalDateTime DEAL_STARTS_AT = LocalDateTime.parse("2022-03-27T17:05:55");
    private static final LocalDateTime DEAL_ENDS_AT = LocalDateTime.parse("2022-03-27T23:05:55");

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DealService dealService;

    @BeforeEach
    public void setUp() {

        when(dealService.addDeal(DealForm.builder()
                .title("Встреча с заказчиком")
                .startsAt(DEAL_STARTS_AT)
                .endsAt(DEAL_ENDS_AT)
                .userIds(new Long[] {1L, 2L})
                    .build()))
                .thenReturn(DealDto.builder()
                        .title("Встреча с заказчиком")
                        .startsAt(DEAL_STARTS_AT)
                        .endsAt(DEAL_ENDS_AT)
                        .userIds(new HashSet<>(Arrays.asList(1L, 2L)))
                            .build());

        when(dealService.addDeal(DealForm.builder()
                .title("Встреча с заказчиком")
                .startsAt(DEAL_STARTS_AT)
                .endsAt(DEAL_ENDS_AT)
                .userIds(new Long[] {1L, 2L, 3L})
                .build())).thenThrow(new EntityNotFoundException());

        when(dealService.getCorporateFreeGaps(Arrays.asList(1L, 2L)))
                .thenReturn(Arrays.asList(
                        new LocalDateTime[] {
                                LocalDateTime.parse("2022-03-27T00:00:00"),
                                LocalDateTime.parse("2022-03-27T12:05:55")
                        },
                        new LocalDateTime[] {
                                LocalDateTime.parse("2022-03-27T15:05:55"),
                                LocalDateTime.parse("2022-03-28T00:00:00")
                        }
                ));

        when(dealService.getCorporateFreeGaps(Collections.singletonList(1L)))
                .thenReturn(Arrays.asList(
                        new LocalDateTime[] {
                                LocalDateTime.parse("2022-03-27T00:00:00"),
                                LocalDateTime.parse("2022-03-27T12:05:55")
                        },
                        new LocalDateTime[] {
                                LocalDateTime.parse("2022-03-27T14:05:55"),
                                LocalDateTime.parse("2022-03-28T00:00:00")
                        }
                ));

        when(dealService.getCorporateFreeGaps(Collections.emptyList()))
                .thenThrow(new IllegalArgumentException("Список id пуст"));

    }

    @Nested
    @DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
    @DisplayName("attachDealToUsers() is working")
    class AttachDealToUsersTests {

        @Test
        public void deal_is_attached_when_form_is_valid_and_users_exist() throws Exception {
            mockMvc.perform(post("/deal")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content("{\n" +
                                    "    \"title\": \"Встреча с заказчиком\",\n" +
                                    "    \"startsAt\": \"2022-03-27T17:05:55\",\n" +
                                    "    \"endsAt\": \"2022-03-27T23:05:55\",\n" +
                                    "    \"userIds\": [1, 2]\n" +
                                    "}"))
                    .andDo(print())
                    .andExpect(jsonPath("title", is("Встреча с заказчиком")))
                    .andExpect(jsonPath("startsAt", is("2022-03-27T17:05:55")))
                    .andExpect(jsonPath("endsAt", is("2022-03-27T23:05:55")))
                    .andExpect(jsonPath("userIds[0]", is(1)))
                    .andExpect(jsonPath("userIds[1]", is(2)));
        }

        @Test
        public void bad_request_when_invalid_date() throws Exception {
            mockMvc.perform(post("/deal")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\n" +
                            "    \"title\": \"Встреча с заказчиком\",\n" +
                            "    \"startsAt\": \"2022-03-27 17:05:55\",\n" +
                            "    \"endsAt\": \"2022-03-27T23:05:55\",\n" +
                            "    \"userIds\": [1, 2]\n" +
                            "}"))
                    .andDo(print())
                    .andExpect(status().isBadRequest());
        }

        @Test
        public void bad_request_when_user_does_not_exist() throws Exception {
            mockMvc.perform(post("/deal")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content("{\n" +
                                    "    \"title\": \"Встреча с заказчиком\",\n" +
                                    "    \"startsAt\": \"2022-03-27 17:05:55\",\n" +
                                    "    \"endsAt\": \"2022-03-27T23:05:55\",\n" +
                                    "    \"userIds\": [1, 2, 3]\n" +
                                    "}"))
                            .andDo(print())
                            .andExpect(status().isBadRequest());
        }

    }

    @Nested
    @DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
    @DisplayName("getCorporateFreeGaps() is working")
    class GetCorporateFreeGapsTests {

        @Test
        public void return_corporate_free_gaps_if_they_exist() throws Exception {
            mockMvc.perform(get("/deal")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("[1, 2]"))
                    .andDo(print())
                    .andExpect(jsonPath("$[0][0]", is("2022-03-27T00:00:00")))
                    .andExpect(jsonPath("$[0][1]", is("2022-03-27T12:05:55")))
                    .andExpect(jsonPath("$[1][0]", is("2022-03-27T15:05:55")))
                    .andExpect(jsonPath("$[1][1]", is("2022-03-28T00:00:00")));
        }

        @Test
        public void return_free_gaps_if_one_user() throws Exception {
            mockMvc.perform(get("/deal")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content("[1]"))
                            .andDo(print())
                            .andExpect(jsonPath("$[0][0]", is("2022-03-27T00:00:00")))
                            .andExpect(jsonPath("$[0][1]", is("2022-03-27T12:05:55")))
                            .andExpect(jsonPath("$[1][0]", is("2022-03-27T14:05:55")))
                            .andExpect(jsonPath("$[1][1]", is("2022-03-28T00:00:00")));
        }

        @Test
        public void bad_request_if_id_list_is_empty() throws Exception {
            mockMvc.perform(get("/deal")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content("[]"))
                    .andDo(print())
                    .andExpect(status().isBadRequest());
        }

    }

}
